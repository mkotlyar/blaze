""" Utility functions
"""

import gdb
import re


def stripType(type):
	"""Returns type with references, qualifiers, and typedefs removed"""

	if type.code == gdb.TYPE_CODE_REF:
		type = type.target()

	return type.unqualified().strip_typedefs()


def templateParams(type):
	"""Return template parameters as a list of strings

	The gdb extension does not support value template arguments -- need to extract them by hand.
	"""

	m = re.match(r'[\w:]+<(.*)>', type.tag)
	if m:
		template_params = re.findall(r'[\w:]+(?:<.*>)?', m[1])
		template_params = [x.replace(" ", "") for x in template_params]
		return template_params
	else:
		return None


def strToBool(s):
	"""Convert 'true' or 'false' string to a bool value
	"""

	if s == 'true':
		return True
	elif s == 'false':
		return False
	else:
		raise ValueError('Boolean value must be "true" or "false"')


def dataPtrFromAlignedArray(aligned_array):
	"""Get data pointer from an AlignedArray object
	"""
	element_type = aligned_array.type.template_argument(0)
	return aligned_array['v_'].cast(element_type.pointer())


def cppLiteralToInt(literal: str) -> int:
    """ Remove C++ integer suffixes (U, L, UL, LL, ULL, etc.)
	"""
    stripped = re.sub(r'[uUlL]+$', '', literal)

    # Detect hexadecimal, octal, or binary literals and convert accordingly
    if stripped.startswith("0x") or stripped.startswith("0X"):
        return int(stripped, 16)
    elif stripped.startswith("0b") or stripped.startswith("0B"):
        return int(stripped, 2)
    elif stripped.startswith("0") and stripped != "0":  # Octal (but not just "0")
        return int(stripped, 8)

	 # Default to decimal conversion
    return int(stripped)
